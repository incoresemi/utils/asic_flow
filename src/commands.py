import os
import sys
import subprocess
import logging
# ########### Generate hier.txt #########################################

logger= logging.getLogger(__name__)
def get_hierarchy(verilogdir, topmodule, prefix):
    currdir_path = os.getcwd()
    logger.info("Generating hierarchy for")
    logger.info("Module: " + topmodule)
    logger.info("Directory: " + verilogdir)
    subprocess.call("rm -rf rtl", shell=True)
    subprocess.call("cp -r " + verilogdir + " rtl", shell=True)
    os.chdir(currdir_path + "/src")
    if not(prefix=='None'):
        subprocess.call("perl hier_print.pl -t " + topmodule + " ../rtl/*.v |\
        grep " + prefix + " |tac > ../hier.txt;", shell=True)
    else:
        subprocess.call("perl hier_print.pl -t " + topmodule + " ../rtl/*.v |\
        tac > ../hier.txt;", shell=True)

    count = len(open('../hier.txt').readlines())
    if(count == 0):
        print("ERROR: Module " + topmodule + 
        " not found in directory or module with prefix "+prefix+" not found")
        sys.exit(0)
    os.chdir(currdir_path)
    return 0
# #######################################################################


# ############# create directory structure first. ########################
def create_directories(hierfile):
    if(hierfile is None):
        hierfile = 'hier.txt'
    logger.info("Creating hierarchical directory structure")
    inputfile = open(hierfile, 'r')
    for lineno, line in enumerate(inputfile):
        line = line.lstrip()
        line = line.rstrip("\n")
        subprocess.call("mkdir rtl/" + line, shell=True)
        subprocess.call("mv rtl/" + line + ".v rtl/" + line, shell=True)

    subprocess.call("mkdir rtl/common", shell=True)
    subprocess.call("mv rtl/*.v rtl/common", shell=True)
    inputfile.close()
    return 0
# #########################################################################

# ############# create tcl files for each module ##########################


def create_tcl(tool, hierfile):
    logger.info("Generating TCL scripts")
    currdir_path = os.getcwd()
    if(hierfile is None):
        hierfile = 'hier.txt'
    inputfile = open(hierfile, 'r')
    script_names = [
        'appvariables.tcl',
        'dc.tcl',
        'path_groups.tcl',
        'constraints.sdc']

    hierarchy = [[] for i in range(7)]
    prevlevel = 8
    for lineno, line in enumerate(inputfile):
        line = line.rstrip("\n")
        hier_level = int((len(line) - len(line.lstrip())) / 2)
        module_name = line.lstrip()
        # the following will clear hierarchy which does not belong to this
        # module.
        if(prevlevel <= hier_level):
            for i in range(prevlevel + 1, len(hierarchy)):
                hierarchy[i].clear()
        prevlevel = hier_level

        # append the current module to its hierarchy list.
        hierarchy[hier_level].append(module_name)

        # copy synthesis scripts to the module folders
        for i in range(len(script_names)):
            subprocess.call("cp templates_"+tool+"/{0}\
                rtl/{1}".format(script_names[i], module_name), shell=True)

        # find the submodules of the current module. Create a dont_touch and
        # ddc_read for these modules.
        analyse_module = open(
            'rtl/' +
            module_name +
            '/analyze_module.tcl',
            'w')
        analyse_module.write(
            "set design " + module_name + "\n" +
            "set RTLCOMMON " + currdir_path + "/rtl/common\n" +
            "analyze -format verilog -autoread " + currdir_path + "/rtl/" +
            module_name +
            "/" +
            module_name +
            ".v\n")
        analyse_module.close()
        for i in range(hier_level + 1, hier_level + 3):
            ddc_file = open('rtl/' + module_name + '/read_ddc.tcl', 'w')
            dont_touch_file = open(
                'rtl/' + module_name + '/dont_touch.tcl', 'w')
            if not hierarchy[i]:
                break
            else:
                for j in range(len(hierarchy[i])):
                    ddc_file.write("read_ddc "+currdir_path+"/rtl/" +
                                   str(hierarchy[i][j]) +
                                   "/outputs/" +
                                   str(hierarchy[i][j]) +
                                   ".ddc\n")
                    dont_touch_file.write(
                        "set_dont_touch [get_designs *" + str(hierarchy[i][j])
                        + "*]\n")
                break
            ddc_file.close()
            dont_touch_file.close()
    inputfile.close()
    return 0
# ############################################################################

# ######### perform synthesis ################################################
def perform_synthesis(hierfile, env):
    if(hierfile is None):
        hierfile = 'hier.txt'
    currdir_path = os.getcwd()
    inputfile=open(hierfile,'r')
    for lineno,line in enumerate(inputfile):
        module=line.lstrip()
        module=module.rstrip("\n")
        logger.info("Synthesizing module: "+module)
        logger.debug("using env: "+env)
        os.chdir(currdir_path+"/rtl/"+module)
        subprocess.call("cp " + env + " ./environment.tcl",shell=True)
        subprocess.call("dc_shell-xg-t -f dc.tcl > log",shell=True)
        subprocess.call("mkdir meta",shell=True)
        subprocess.call("mv *.pvl *.syn *.mr meta",shell=True)
        print(os.getcwd())
        x=subprocess.Popen(['grep','-i','unresolved\|error\|cannot','log'],
                           stdout= subprocess.PIPE)
        out,err=x.communicate()
        if len(out)!=0:
            print('ERROR: Synthesis log of module: '+module+' has errors')
            print(str(out))
            sys.exit(0)
        os.chdir(currdir_path)
    logger.info("Synthesis done")
    return 0
# ############################################################################


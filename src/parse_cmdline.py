import os
import sys
import subprocess
import argparse
import importlib
import operator

class SortingHelpFormatter(argparse.HelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=operator.attrgetter('option_strings'))
        super(SortingHelpFormatter, self).add_arguments(actions)

def cmdline_args():

    parser = argparse.ArgumentParser(
        formatter_class=SortingHelpFormatter,
        prog="Hierarchical Synthesis",
        description="Tool to perform hierarchical synthesis given a verilog\
          folder and a top module name."
    )

    parser.add_argument(
        '--verilog-dir',
        type=str,
        metavar='FOLDER',
        help='Path to the folder containing the verilog files',
    )

    parser.add_argument(
        '--top',
        metavar='TOPMODULE',
        type=str,
        help='Name of the top module to be synthesized'
    )

    parser.add_argument(
        '--tool',
        type=str,
        default='dc',
        help='Synthesis Tools (dc or rc) (default: dc)'
    )

    parser.add_argument(
        '--prefix',
        type=str,
        default='mk',
        help='Prefix to filter modules name nodes (default: mk). Use "None" to\
        print the entire hierarchy'
    )
    parser.add_argument(
        '--hierfile',
        type=str,
        metavar='FILE',
        default=None,
        help='hierarchical file for parsing (default: hier.txt)'
    )
    parser.add_argument(
        '--version',
        action='version',
        version='0.4.0'
    )
    parser.add_argument(
        '--gen-hier-only',
        action='store_true',
        help='Generate only the hierarchical file (reverse order)'
    )
    parser.add_argument(
        '--create-tcl-only',
        action='store_true',
        help='Crete RTL folders with TCL scripts. Do not start synthesis'
    )
    parser.add_argument(
        '--clean',
        action='store_true',
        help='clean all build directories'
    )
    parser.add_argument(
        '--synth',
        action='store_true',
        help='perform synthesis'
    )
    parser.add_argument(
        '--env',
        type=str,
        metavar='TCL_FILE',
        default=None,
        help='The environment tcl file with library settings. The file has to\
        be prefixed with the absolute path of the file. Please check the\
        environment.tcl in templates folder for an example'
    )

    parser.add_argument(
        '--verbose',
        action= 'store',
        default='info',
        help='debug | info | warning | error', 
        metavar=""
    )
    return parser

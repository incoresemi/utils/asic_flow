import logging
import subprocess
import select
class ColoredFormatter(logging.Formatter):                                      
    """                                                                         
        Class to create a log output which is colored based on level.           
    """                                                                         
    def __init__(self, *args, **kwargs):                                        
        super(ColoredFormatter, self).__init__(*args, **kwargs)                 
        self.colors = {                                                         
                'DEBUG' : '\033[94m',                                           
                'INFO'  : '\033[92m',                                           
                'WARNING' : '\033[93m',                                         
                'ERROR' : '\033[91m',                                           
        }                                                                       
                                                                                
        self.reset = '\033[0m'                                                  
                                                                                
    def format(self, record):                                                   
        msg = str(record.msg)                                                   
        level_name = str(record.levelname)                                      
        name = str(record.name)                                                 
        color_prefix = self.colors[level_name]                                  
        return '{0}{1:<9s} - {2:<25s} : {3}{4}'.format(                         
                color_prefix,                                                   
                '[' + level_name + ']',                                         
                name,                                                           
                msg,                                                            
                self.reset)                 

                                                                                
def setup_logging(log_level):                                                   
    """Setup logging                                                            
                                                                                
        Verbosity decided on user input                                         
                                                                                
        Args:                                                                   
            log_level: (str) User defined log level                             
                                                                                
        Returns:                                                                
            None                                                                
    """                                                                         
    numeric_level = getattr(logging, log_level.upper(), None)                   
                                                                                
    if not isinstance(numeric_level, int):                                      
        print("\033[91mInvalid log level passed. Please select from debug | info | warning | error\033[0m")
        sys.exit(1)                                                             
                                                                                
    logging.basicConfig(level = numeric_level)

def call(popenargs, logger, stdout_log_level=logging.DEBUG,
                                    stderr_log_level=logging.ERROR, **kwargs):
    """
    Variant of subprocess.call that accepts a logger instead of stdout/stderr,
    and logs stdout messages via logger.debug and stderr messages via
    logger.error.
    """
    child = subprocess.Popen(popenargs, stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, **kwargs)

    log_level = {child.stdout: stdout_log_level,
                 child.stderr: stderr_log_level}

    def check_io():
        ready_to_read = select.select([child.stdout, child.stderr], [], [], 1000)[0]
        for io in ready_to_read:
            line = io.readline()
            logger.log(log_level[io], line[:-1])

    # keep checking stdout/stderr until the child exits
    while child.poll() is None:
        check_io()

    check_io()  # check again to catch anything after the process exits

    return child.wait()

# import library modules
import os
import sys
import subprocess
import argparse
import logging
# import local modules
import commands
import parse_cmdline
import utils

parser = parse_cmdline.cmdline_args()

args = parser.parse_args()

utils.setup_logging(args.verbose)
logger=logging.getLogger()
logger.handlers = []
ch=logging.StreamHandler()
ch.setFormatter(utils.ColoredFormatter())
logger.addHandler(ch)


doall = False
if (not args.create_tcl_only and not args.gen_hier_only):
    doall = True

if (args.clean):
    subprocess.call('rm -rf rtl hier.txt src/__pycache__', shell=True)
    logger.info("Setup is clean")
    sys.exit(0)

if (args.verilog_dir is None):
    parser.error("please provide a valid Verilog folder")
    sys.exit(0)

if (args.top is None):
    parser.error("please provide a valid Top Module Name")
    sys.exit(0)

if (args.gen_hier_only):
    commands.get_hierarchy(args.verilog_dir, args.top, args.prefix)
    sys.exit(0)

if (args.create_tcl_only or doall):
    commands.get_hierarchy(args.verilog_dir, args.top, args.prefix)
    commands.create_directories(args.hierfile)
    commands.create_tcl(args.tool, args.hierfile)
    if(args.create_tcl_only):
        sys.exit(0)

if (args.synth):
    if(args.env is None):
      parser.error("Please provide an envionrment tcl file using the \
--env arguement")
    commands.perform_synthesis(args.hierfile, args.env)

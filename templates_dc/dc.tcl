
source environment.tcl 

######### Setting environment variables ###########
set DATE [clock format [clock seconds] -format "%b%d-%T"]
set OUTPUTS outputs
set REPORTS reports
set link_library      [concat $link_library $target_library $synthetic_library ]
####################################################


if {![file exists ${OUTPUTS}]} {
  file mkdir ${OUTPUTS}
  puts "Creating directory ${OUTPUTS}"
}

if {![file exists ${REPORTS}]} {
  file mkdir ${REPORTS}
  puts "Creating directory ${REPORTS}"
}

# for formality

source analyze_module.tcl
analyze -format verilog -recursive -autoread $RTLCOMMON
source read_ddc.tcl

elaborate $design
set_svf ${OUTPUTS}/$design.svf

#----------------------------
# current_design 
#----------------------------
current_design $design
link
source dont_touch.tcl
## uniquify is done automatically by compile_ultra
#----------------------------
# dc setting 
#----------------------------
source appvariables.tcl

#----------------------------
# constraint settings 
#----------------------------
source constraints.sdc 
set_max_area 0
set_dont_retime $design true

#----------------------------
# path group settings 
#----------------------------
source path_groups.tcl
#----------------------------
# sanity check 
#----------------------------
check_design                             > ${REPORTS}/$design.check_design.pre_compile.rpt
report_net_fanout -threshold 50 -nosplit > ${REPORTS}/$design.fanout.pre_compile.rpt
#----------------------------
# misc constraints 
#----------------------------
set verilogout_no_tri  "true"

##----------------------------
## compile 
##----------------------------
compile_ultra -no_autoungroup -no_boundary_optimization  -no_seq_output_inversion 
write -f ddc     -hier -o   ${OUTPUTS}/$design.ddc
write -f verilog -hier -o   ${OUTPUTS}/$design.v

#----------------------------
# checkpoint options 
#----------------------------
define_name_rules verilog -allowed "a-z A-Z 0-9 _ /"
report_name_rules verilog 
# variable setting for verilog out
change_names -hier -rules verilog

#----------------------------
# checkpoint
#----------------------------
write -f ddc     -hier -o   ${OUTPUTS}/$design.flat.ddc
write -f verilog -hier -o   ${OUTPUTS}/$design.flat.v
report_reference -nosplit > ${REPORTS}/$design.ref.flat.rpt
write_sdc ${OUTPUTS}/$design.sdc
write_sdf ${OUTPUTS}/$design.sdf
#write_test_model -format ctl -output ${OUTPUTS}/$design.flat.ctl

#---------------------------
# report generation 
#---------------------------
report_qor > ${REPORTS}/$design.qor
report_timing -nosplit -max_paths 5000 -input -nets -cap -tran -nosplit -sig 3                 > ${REPORTS}/$design.setup.all.tim.rpt.by_group
report_timing -nosplit -max_paths 5000 -input -nets -cap -tran -nosplit -sig 3 -sort_by slack  > ${REPORTS}/$design.setup.all.tim.rpt.by_slack
report_timing -nosplit -max_paths 2000 -input -nets -cap -tran -nosplit -sig 3 -group f2f      > ${REPORTS}/$design.setup.f2f.tim.rpt
report_timing -nosplit -max_paths 2000 -input -nets -cap -tran -nosplit -sig 3 -group i2f      > ${REPORTS}/$design.setup.i2f.tim.rpt
report_timing -nosplit -max_paths 2000 -input -nets -cap -tran -nosplit -sig 3 -group f2o      > ${REPORTS}/$design.setup.f2o.tim.rpt
report_timing -nosplit -max_paths 2000 -input -nets -cap -tran -nosplit -sig 3 -group i2o      > ${REPORTS}/$design.setup.i2o.tim.rpt


report_area -nosplit -hier										 > ${REPORTS}/$design.area.rpt
report_clock_gating -verbose -gated  -gating_elements  > ${REPORTS}/$design.clock_gate.rpt
check_design													    > ${REPORTS}/$design.check_design.rpt
report_net_fanout -threshold 50 -nosplit					 > ${REPORTS}/$design.fanout.rpt
check_timing													    > ${REPORTS}/$design.check_timing.rpt
report_resources -nosplit -hierarchy					    > ${REPORTS}/$design.report_resources.compile_1
report_power														 > ${REPORTS}/$design.power.rpt

create_block_abstraction
write -f ddc -hier -o ${OUTPUTS}/$design.abstraction.ddc
exit
